<?php
  abstract class Role
  {
    const Teacher = 'teacher';
    const Student = 'student';
    const Parent = 'parent';
  }
?>