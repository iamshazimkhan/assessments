<?php
require_once('Role.php');

class User
{
  private $user_id;
  private $first_name;
  private $last_name;
  private $email;
  private $photo;
  private $role;
  private $salutation;

  const DEFAULT_AVATAR = "path_to_default_avatar";
  protected static $ID = 0;

  public function __construct($first_name, $last_name, $email, $role, $salutation = null, $photo = null)
  {
    $this->setFirstName($first_name);
    $this->setLastName($last_name);
    $this->setEmail($email);
    $this->setRole($role);
    $this->setSalutation($salutation);
    $this->setPhoto($photo);
    $this->setUserID();

    return "User is successfully saved.";
  }

  // Setters

  protected function setFirstName($first_name) {
    if (empty($first_name)) {
      throw new InvalidArgumentException(
        'First name is required'
      );
    }
    $this->first_name = $first_name;
  }

  protected function setLastName($last_name) {
    if (empty($last_name)) {
      throw new InvalidArgumentException(
        'Last name is required'
      );
    }
    $this->last_name = $last_name;
  }

  protected function setEmail($email) {
    if (empty($email)) {
      throw new InvalidArgumentException(
        'Email is required'
      );
    }
    $this->email = $email;
  }

  protected function setSalutation($salutation) {
    $this->salutation = $salutation;
  }

  protected function setRole($role) {
    if (empty($role)) {
      throw new InvalidArgumentException(
        'Role is required'
      );
    }

    $roles = array(Role::Teacher, Role::Student, Role::Parent);

    if (!in_array($role, $roles)) {
      throw new InvalidArgumentException(
        '$role has not a valid value'
      );
    }
    $this->role = $role;
  }

  protected function setPhoto($photo) {

    if ($photo != null && !str_ends_with($photo, ".jpg")) {
      throw new InvalidArgumentException(
        'Please provide a valid photo. Only .jpg supported'
      );
    }
    $this->photo = $photo;
  }

  protected function setUserID() {
    self::$ID = self::$ID + 1;
    $this->user_id = self::$ID;
  }

  // Getters
  
  public function getFullName()
  {
    switch ($this->role) {
      case Role::Teacher:
        return $this->salutation + " " + $this->first_name + " " + $this->last_name;
        break;
      case Role::Parent:
        return $this->salutation + " " + $this->first_name + " " + $this->last_name;
        break;
      case Role::Student:
        return $this->first_name + " " + $this->last_name;
        break;
      default:
        return $this->first_name + " " + $this->last_name;
    }
  }

  public function getProfilePhoto() {
    return $this->photo ? $this->photo : self::DEFAULT_AVATAR; 
  }

  public function getUserID() {
    return $this->user_id;
  }

  public function getEmail() {
    return $this->$email;
  }

  public function getRole() {
    return $this->$role;
  }
}
?>