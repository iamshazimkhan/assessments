<?php
require_once('User.php');
require_once('Role.php');
require_once('MessageType.php');

class Message
{
  private $message_id;
  private $sender;
  private $receiver;
  private $text;
  private $type;
  private $time;

  protected static $ID = 0;

  public function __construct($type, User $sender, User $receiver, $text)
  {
    $this->setType($type);
    $this->setSender($sender);
    $this->setReceiver($receiver);
    $this->setText($text);
    $this->setTime();
    $this->setMessageID();

    return "Message successfully saved.";
  }

  protected function setMessageID() {
    self::$ID = self::$ID + 1;
    $this->message_id = self::$ID;
  }

  protected function setSender($sender) {
    if (empty($sender)) {
      throw new InvalidArgumentException(
        'Sender is required'
      );
    }
    $this->sender = $sender;
  }

  protected function setReceiver($receiver) {
    if (empty($receiver)) {
      throw new InvalidArgumentException(
        'Receiver is required'
      );
    }
    $this->receiver = $receiver;
  }
  
  protected function setType($type) {
    if (empty($type)) {
      throw new InvalidArgumentException(
        'Message Type is required'
      );
    }
    $types = array(MessageType::System, MessageType::manual);

    if (!in_array($type, $types)) {
      throw new InvalidArgumentException(
        '$type has not a valid value'
      );
    }
    $this->type = $type;
  }

  protected function setText($text) {
    if($this->type == MessageType::System && $this->sender->getRole() != Role::Teacher) {
      throw new InvalidArgumentException(
        'Only Teacher can send System Message'
      );
    }

    if($this->type == MessageType::System && $this->receiver->getRole() != Role::Student) {
      throw new InvalidArgumentException(
        'Only Student can receive System Message'
      );
    }

    $this->text = $text;
  }

  protected function setTime() {
    $this->time = date('d-m-y h:i:s');
  }

  public function getSenderName()
  {
    return $this->sender->getFullName();
  }

  public function getReceiverName()
  {
    return $this->receiver->getFullName();
  }

  public function getText()
  {
    return $this->$text;
  }

  public function getType()
  {
    return $this->$type;
  }

  public function getTime()
  {
    return $this->$time;
  }
}

?>